# This script will hide the Win 11 ready notification MS is pushing to our SCCM managed Win 10 machines
# Start Log
Start-Transcript -path C:\Temp\Win11UXsettings\log.txt -Force
#Variables
$RegistryPath = 'HKLM:\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings'
# If registry path does not exist then creat it
If  (-NOT (Test-Path $RegistryPath)) {
    Write-Output "Reg path doesnt exist. Creating Reg Key - Settings"
    New-Item -Path $RegistryPath -Force | Out-Null
}
# Create or set value for SvOfferDeclined property in Settings key
Write-Output "Creating/Setting SvOfferDeclined property "
Set-Itemproperty $RegistryPath SvOfferDeclined 0x1646085160366 -type QWord
# End Log
Stop-Transcript